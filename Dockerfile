FROM debian:bullseye-slim

USER root

RUN apt update && apt full-upgrade -y && apt install ansible git -y

